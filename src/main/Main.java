package main;

public class Main {
	public static void main(String[] args) {
		String original = "abcde";
		for (int i = 0; i < original.length(); i++) {
			System.out.println("The character at index " + i + " is " + original.charAt(i));
		}
		System.out.println("The letter 'a' is at index " + original.indexOf('a'));
		System.out.println("The letter 'b' is at index " + original.indexOf('b'));
		System.out.println("The letter 'A' is at index " + original.indexOf('A'));
		
		original = original.toUpperCase();
		System.out.println(original);
		System.out.println("The letter 'A' is at index " + original.indexOf('A'));
	}
}
